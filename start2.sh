#!/bin/sh
export PATH=$PATH:/tmp/maria/bin

cd /tmp/maria
rm /tmp/data2/core.*
exec mysqld_safe --defaults-file=~/dev/maria/my2.cnf
#--skip-grant-tables
#--debug=d,info,error,query,general,where:O,/tmp/mysqld.trace --binlog-ignore-db=testdatabase
