#!/bin/bash
export PATH=$PATH:/tmp/maria/bin

mycnf=$(pwd)/my.cnf

cd /tmp/maria
rm /tmp/data/core.*
exec mysqld_safe --defaults-file=$mycnf
